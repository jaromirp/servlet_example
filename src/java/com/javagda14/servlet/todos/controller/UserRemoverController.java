package com.javagda14.servlet.todos.controller;

import com.javagda14.servlet.todos.service.DependencyManager;
import com.javagda14.servlet.todos.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.registry.infomodel.User;
import java.io.IOException;

@WebServlet("/user/remove")
public class UserRemoverController extends HttpServlet {

    private UserService userService;

    public UserRemoverController() {
        userService = DependencyManager.getInstance().getBean(UserService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (req.getParameter("id") != null) {
            int id = Integer.parseInt(req.getParameter("id"));
            userService.removeUserWithId(id);

        }
        resp.sendRedirect("/user/list");
    }
}
