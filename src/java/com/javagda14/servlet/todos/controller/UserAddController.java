package com.javagda14.servlet.todos.controller;

import com.javagda14.servlet.todos.model.AppUser;
import com.javagda14.servlet.todos.service.DependencyManager;
import com.javagda14.servlet.todos.service.UserService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/user/add")
public class UserAddController extends HttpServlet {


    private UserService userService;
    private int licznik_id = 0;

    public UserAddController(){
        userService = DependencyManager.getInstance().getBean(UserService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        super.doGet(req, resp);
//        req.setAttribute("licznik", licznik++);
        req.getRequestDispatcher("/user/user_form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String passowrd = req.getParameter("password");
        String passowrd_confirm = req.getParameter("password-confirm");

        if (username.isEmpty() || passowrd.isEmpty() || !passowrd.equals(passowrd_confirm)) {
            req.setAttribute("error_message", "Incorrect username or password!");

            req.getRequestDispatcher("/user/user_form.jsp").forward(req, resp);
            return;
        }
        //stworzyć user'a i ustwić mu wszystkie parametry
        AppUser newUser = new AppUser(licznik_id++, username, passowrd);
        userService.addUser(newUser);
        resp.sendRedirect("/user/list");
    }
}
