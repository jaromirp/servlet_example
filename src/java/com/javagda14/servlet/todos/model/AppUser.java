package com.javagda14.servlet.todos.model;

public class AppUser {
    private int id;
    private String login;
    private String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AppUser(int id, String login, String password) {

        this.id = id;
        this.login = login;
        this.password = password;
    }
}
